import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

class Square extends React.Component {
  render() {
    const style = this.props.highlight ? {background: 'yellow'} : {};
    return (
      <button
        className="square"
        style={style}
        onClick={() => {this.props.onClick()}}
      >
        {this.props.value}
      </button>
    );
  }
}

class Board extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      squares: Array(9).fill(null),
      xIsNext: true,
    };
  }

  renderSquare(i, highlight) {
    return (
      <Square key={i}
        value={this.props.squares[i]}
        highlight={highlight}
        onClick={() => {this.props.onClick(i)}}
      />
    );
  }

  render() {
    let rows = []

    for (let r = 0; r < 3; r++) {
      let row = []
      for (let c = 0; c < 3; c++) {
        let i = r * 3 + c
        row.push(this.renderSquare(i, (this.props.line.includes(i))));
      }
      rows.push(<div key={r} className="board-row">{row}</div>)
    }

    return (
      <div>{rows}</div>
    );
  }
}

class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      history: [{
        squares: Array(9).fill(null),
      }],
      stepNumber: 0,
      xIsNext: true,
      sortHistoryDescending: true,
    }
  }


  currentPlayer() {
    return this.state.xIsNext ? 'X' : 'O';
  }

  handleClick(i) {
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1]
    const squares = current.squares.slice();
    if (calculateWinner(squares) || squares[i]) {
      return;
    }
    squares[i] = this.currentPlayer();
    this.setState({
      history: history.concat([{
        squares: squares,
        location: {
          row: Math.floor(i/3),
          col: i % 3,
        }
      }]),
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext,
    });
  }

  jumpTo(step) {
    this.setState({
      stepNumber: step,
      xIsNext: (step % 2) === 0,
    })
  }

  render() {
    const history = this.state.history;
    const current = history[this.state.stepNumber];
    const winner = calculateWinner(current.squares);

    const moves = history.map((step, move) => {
      let desc = move ? `go to move #${move} (${step.location.col},${step.location.row})`: `go to start`;
      let style = (move === this.state.stepNumber) ? {fontWeight: 'bold'} : {};
      return (
        <li key={move * (this.state.sortHistoryDescending ? 1 : -1)}>
          <button
              onClick={() => this.jumpTo(move)}
              style={style}
          >
            {desc}
          </button>
        </li>
      );
    });

    let status;
    let line = [];
    let draw = false;
    
    if (winner) {
      status = `Winner: ${winner.player}`
      line = winner.line
    } else {
      draw = !current.squares.some((i)=> {return i === null})
      status = draw ? 'Draw!' : `Next player: ${this.currentPlayer()}`
    }

    return (
      <div className="game">
        <div className="game-board">
          <Board
            squares={current.squares}
            line={line}
            onClick={(i) => this.handleClick(i)}
          />
        </div>
        <div className="game-info">
          <div>{status}</div>
          History
          <button
            onClick={() => this.setState({sortHistoryDescending: !this.state.sortHistoryDescending})}
          >
          {this.state.sortHistoryDescending ? 'desc' : 'asc'}
          </button>
          <ol>{this.state.sortHistoryDescending ? moves : moves.reverse()}</ol>
        </div>
      </div>
    );
  }
}

ReactDOM.render(
  <Game />,
  document.getElementById('root')
);

function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return { player: squares[a], line: lines[i] };
    }
  }
  return null;
}